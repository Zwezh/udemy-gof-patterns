package by.zwezh;

import by.zwezh.patterns.bridge.Bridge;
import by.zwezh.patterns.builder.Builder;
import by.zwezh.patterns.builder.Person;
import by.zwezh.patterns.builder.PersonBuilder;
import by.zwezh.patterns.chainOfResponsability.ChainOfResponsability;
import by.zwezh.patterns.command.Command;
import by.zwezh.patterns.composit.Composit;
import by.zwezh.patterns.decorator.Decorator;
import by.zwezh.patterns.fabricAndAbstract.FabricAndAbstract;
import by.zwezh.patterns.facade.Facade;
import by.zwezh.patterns.flyWeight.FlyWeight;
import by.zwezh.patterns.interpreter.Interpreter;
import by.zwezh.patterns.iterator.Iterator;
import by.zwezh.patterns.mediator.Mediator;
import by.zwezh.patterns.memento.Memento;
import by.zwezh.patterns.observer.Observer;
import by.zwezh.patterns.prototype.Prototype;
import by.zwezh.patterns.proxy.Proxy;
import by.zwezh.patterns.singleton.Singleton;
import by.zwezh.patterns.state.State;
import by.zwezh.patterns.strategy.Strategy;
import by.zwezh.patterns.template.Template;
import by.zwezh.patterns.visitor.Visitor;

public class Main {

    public static void main(String[] args) {
//        new Adapter();
//        new Bridge();
//        new Builder().setName("Alex").setAge(30).built().print();
//        new ChainOfResponsability();
//        new Command();
//        new Composit();
//        new Decorator();
//        new Facade();
//        new FabricAndAbstract();
//        new FlyWeight();
//        new Interpreter();
//        new Iterator();
//        new Mediator();
//        new Memento();
//        new Observer();
//        new Prototype();
//        new Proxy();
//        new Singleton();
//        new State();
//        new Strategy();
//        new Template();
        new Visitor();
    }
}
