package by.zwezh.patterns.bridge;

public class Bridge {
    public Bridge() {
        Vehicle toyotaCar = new Car(new Toyota());
        Vehicle audiCar = new Car(new Audi());
        Vehicle audiTrack = new Track(new Audi());
        toyotaCar.drive();
        audiCar.drive();
        audiTrack.drive();
    }
}
