package by.zwezh.patterns.bridge;

public interface Model {
    void drive(String str);
}
