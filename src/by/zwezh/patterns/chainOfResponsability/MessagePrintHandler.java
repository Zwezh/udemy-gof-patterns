package by.zwezh.patterns.chainOfResponsability;

public class MessagePrintHandler extends MessageHandler {

    public MessagePrintHandler(MessageHandler messageHandler) {
        super(messageHandler);
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    @Override
    void handle(String message) {
        System.out.println(message);
    }
}
