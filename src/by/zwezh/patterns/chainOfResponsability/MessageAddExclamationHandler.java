package by.zwezh.patterns.chainOfResponsability;

public class MessageAddExclamationHandler extends MessageHandler{

    public MessageAddExclamationHandler(MessageHandler messageHandler) {
        super(messageHandler);
    }

    @Override
    void handle(String message) {
        messageHandler.handle(message + "!");
    }
}
