package by.zwezh.patterns.chainOfResponsability;

public class ChainOfResponsability {
    public ChainOfResponsability() {
        MessageHandler messageHandler = new MessageAddExclamationHandler(
                new MessageVerifyHandler(
                        new MessagePrintHandler(null)
                )
        );
        messageHandler.handle("Hello");
    }
}
