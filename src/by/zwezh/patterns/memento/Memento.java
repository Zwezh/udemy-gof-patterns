package by.zwezh.patterns.memento;

import java.util.ArrayList;
import java.util.List;

public class Memento {
    public Memento() {
        List<Originator.Momento> list = new ArrayList<Originator.Momento>();
        Originator originator = new Originator();
        originator.setState("one");
        originator.setState("two");
        list.add(originator.saveState());
        originator.setState("three");
        list.add(originator.saveState());
        originator.setState("four");
        list.add(originator.saveState());
        originator.setState("five");
        list.add(originator.saveState());
        originator.setState("six");
        originator.setState("seven");
        list.add(originator.saveState());
        originator.setState("eight");
        list.add(originator.saveState());
        originator.setState("nine");
        System.out.println(originator.state);
        originator.restoreFromMomento(list.get(2));
        System.out.println(originator.state);
    }
}
