package by.zwezh.patterns.builder;

public class Person {
    String name;
    int age;
    double salary;

    public void print(){
        System.out.println(name + " " + age + " " + salary);
    }
}
