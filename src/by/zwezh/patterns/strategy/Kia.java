package by.zwezh.patterns.strategy;

public class Kia extends Car {
    public Kia() {
        super(new FlyCar());
    }

    @Override
    public void run() {
        System.out.println("Kia run");
    }
}
