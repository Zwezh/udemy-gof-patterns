package by.zwezh.patterns.strategy;

public abstract class Car {
    private Flyable flyable;

    public Car(Flyable flyable) {
        this.flyable = flyable;
    }

    public abstract void run();
    public void stop(){
        System.out.println("stop");
    }
    public void fly() {
        flyable.fly();
    }
}
