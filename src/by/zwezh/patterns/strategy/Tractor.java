package by.zwezh.patterns.strategy;

public class Tractor extends Car {
    public Tractor() {
        super(new NoFly());
    }

    @Override
    public void run() {
        System.out.println("Tractor run");
    }
}
