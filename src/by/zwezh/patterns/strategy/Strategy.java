package by.zwezh.patterns.strategy;

public class Strategy {
    public Strategy() {
        Car toyota = new Toyota();
        Car tractor = new Tractor();
        tractor.fly();
        toyota.fly();
    }
}
