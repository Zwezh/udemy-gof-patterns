package by.zwezh.patterns.strategy;

public class NoFly implements Flyable{

    @Override
    public void fly() {
        System.out.println("No fly");
    }
}
