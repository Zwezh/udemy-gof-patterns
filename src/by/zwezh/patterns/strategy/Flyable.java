package by.zwezh.patterns.strategy;

public interface Flyable {
    void fly();
}
