package by.zwezh.patterns.strategy;

public class FlyCar implements Flyable{
    @Override
    public void fly() {
        System.out.println("fly");
    }
}
