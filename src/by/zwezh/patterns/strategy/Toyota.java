package by.zwezh.patterns.strategy;

public class Toyota extends Car {
    public Toyota() {
        super(new FlyCar());
    }

    @Override
    public void run() {
        System.out.println("Toyota run");
    }
}
