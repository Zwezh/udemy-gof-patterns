package by.zwezh.patterns.singleton;

import java.io.Serializable;

public class ASingleton implements Serializable {
    private int i;

    public int getI() {
        return i;
    }

    private volatile static ASingleton instance;

    private ASingleton() {}

    public synchronized static ASingleton getInstance() {
        if(instance == null) {
            synchronized (ASingleton.class) {
                if(instance == null) {
                    instance = new ASingleton();
                }
            }
            if(Thread.currentThread().getName().equals("Thread-0")) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            instance = new ASingleton();
        }
        return instance;
    }
}
