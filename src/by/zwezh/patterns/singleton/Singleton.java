package by.zwezh.patterns.singleton;

import java.io.*;

public class Singleton {
    public Singleton() throws InterruptedException {
        SingletonWrapper singletonWrapper = new SingletonWrapper();
        SingletonWrapper singletonWrapper1 = new SingletonWrapper();
        Thread thread = new Thread(() -> {
            singletonWrapper.aSingleton = ASingleton.getInstance();
            singletonWrapper.enumSingleton = EnumSingleton.INSTANCE;
        });
        Thread thread1 = new Thread(() -> {
            singletonWrapper1.aSingleton = ASingleton.getInstance();
            singletonWrapper1.enumSingleton = EnumSingleton.INSTANCE;
        });
        thread.start();
        thread1.start();
        thread.join();
        thread.join();
        System.out.println("singletonWrapper.aSingleton");
        System.out.println(singletonWrapper.aSingleton == singletonWrapper1.aSingleton);
        System.out.println(singletonWrapper.enumSingleton == singletonWrapper1.enumSingleton);
        ASingleton aSingleton = ASingleton.getInstance();
        ASingleton aSingleton1 = ASingleton.getInstance();
        System.out.println(aSingleton == aSingleton1);
        System.out.println(aSingleton.getI());
        EnumSingleton enumSingleton = EnumSingleton.INSTANCE;
        EnumSingleton enumSingleton1 = EnumSingleton.INSTANCE;
        System.out.println(enumSingleton1 == enumSingleton);

//        try(FileOutputStream fileOutputStream = new FileOutputStream("test.doc")) {
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
//            FileInputStream fileInputStream = new FileInputStream("test.doc");
//            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
//            objectOutputStream.writeObject(aSingleton);
//            aSingleton = (ASingleton) objectInputStream.readObject();
//            System.out.println(aSingleton == aSingleton1);
//            objectOutputStream.writeObject(enumSingleton);
//            enumSingleton = (EnumSingleton) objectInputStream.readObject();
//            System.out.println(enumSingleton1 == enumSingleton);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        ;
    }
}
