package by.zwezh.patterns.observer;

import java.util.Observable;
import java.util.Observer;

public class MySubscriber implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("s3" + arg);
    }
}
