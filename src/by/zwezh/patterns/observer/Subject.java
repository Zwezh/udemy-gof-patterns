package by.zwezh.patterns.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    List<AObsersvable> list = new ArrayList<AObsersvable>();

    public void subscribe(AObsersvable obsersvable) {
        list.add(obsersvable);
    }

    public void unsubscribe(AObsersvable obsersvable) {
        list.remove(obsersvable);
    }

    public void notifySubscribers(String message) {
        list.forEach(item -> item.callMe(message));
    }
}
