package by.zwezh.patterns.observer;

public class SubscriberSecond implements AObsersvable {
    @Override
    public void callMe(String message) {
        System.out.println("s2" + message);
    }
}
