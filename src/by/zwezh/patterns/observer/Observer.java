package by.zwezh.patterns.observer;

public class Observer {
    public Observer() {
        Subject subject = new Subject();
        AObsersvable subscriber = new Subscriber();
        AObsersvable subscriber1 = new Subscriber();
        AObsersvable subscriber2 = new Subscriber();
        subject.subscribe(subscriber);
        subject.subscribe(subscriber1);
        subject.subscribe(subscriber2);
        subject.subscribe(new SubscriberSecond());
        subject.notifySubscribers("Message");
        subject.unsubscribe(subscriber1);
        subject.notifySubscribers("Message 2");
        ASubject aSubject = new ASubject();
        aSubject.addObserver(new MySubscriber());
        aSubject.addObserver(new MySubscriber());
        aSubject.addObserver(new MySubscriber());
        aSubject.setChanged();
aSubject.notifyObservers("Native message");
    }
}
