package by.zwezh.patterns.visitor;

public interface AnimalVisitor {
    void action(Dog dog);
    void action(Cat cat);
}
