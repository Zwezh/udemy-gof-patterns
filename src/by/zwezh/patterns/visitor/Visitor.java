package by.zwezh.patterns.visitor;

public class Visitor {
    public Visitor() {
        Animal dog = new Dog();
        dog.accept(new SoundVisitor());
        dog.accept(new EatVisitor());
    }
}
