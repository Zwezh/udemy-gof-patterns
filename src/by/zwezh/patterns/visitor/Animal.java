package by.zwezh.patterns.visitor;

public interface Animal {
    void accept(AnimalVisitor animalVisitor);
}

