package by.zwezh.patterns.command;

public class MusicCommand implements Executer {
    MusicPlayer musicPlayer;

    public MusicCommand(MusicPlayer musicPlayer) {
        this.musicPlayer = musicPlayer;
    }

    @Override
    public void execute() {
        musicPlayer.playMusic();
    }
}
