package by.zwezh.patterns.command;

public class Command {
    public Command() {
        Executer executer = new LightCommand(new Light());
        Executer executer1 = new LightAndMusicCommand(new Light(), new MusicPlayer());
        Executer executer2 = new PhoneCommand(new Phone(), "Alex");
        Button button = new Button(executer2);
        button.pressButton();
    }
}
