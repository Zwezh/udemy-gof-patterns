package by.zwezh.patterns.command;

public class LightCommand implements Executer {
    Light light;

    public LightCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.switchLight();
    }
}
