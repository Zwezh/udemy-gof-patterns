package by.zwezh.patterns.command;

public class LightAndMusicCommand implements Executer{
    Light light;
    MusicPlayer musicPlayer;

    public LightAndMusicCommand(Light light, MusicPlayer musicPlayer) {
        this.light = light;
        this.musicPlayer = musicPlayer;
    }

    @Override
    public void execute() {
        light.switchLight();
        musicPlayer.playMusic();
    }
}
