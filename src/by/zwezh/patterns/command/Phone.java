package by.zwezh.patterns.command;

public class Phone {
    public void makeCall(String name) {
        System.out.println("Make call to " + name);
    }
}
