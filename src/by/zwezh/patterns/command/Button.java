package by.zwezh.patterns.command;

public class Button {
    Executer executer;

    public Button(Executer executer) {
        this.executer = executer;
    }

    public void pressButton() {
        executer.execute();
    }
}
