package by.zwezh.patterns.command;

public class MusicPlayer {
    public void playMusic() {
        System.out.println("Play music");
    }
}
