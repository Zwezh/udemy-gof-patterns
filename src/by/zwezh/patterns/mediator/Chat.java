package by.zwezh.patterns.mediator;

public interface Chat {
    void sendMessage(String message, Colegue colegue);
    void addColegue(Colegue colegue);
}
