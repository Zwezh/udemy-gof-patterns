package by.zwezh.patterns.mediator;

public abstract class Colegue {
    Chat chat;
    String name;

    public Colegue(Chat chat, String name) {
        this.chat = chat;
        this.name = name;
    }

    abstract void sendMessage(String message);

    abstract void printMessage(String message);
}
