package by.zwezh.patterns.mediator;

public class ColegueImpl extends Colegue {
    public ColegueImpl(Chat chat, String name) {
        super(chat, name);
    }

    @Override
    void sendMessage(String message) {
        chat.sendMessage(message, this);
    }

    @Override
    void printMessage(String message) {
        System.out.println("to " + name +" "+ message);
    }
}
