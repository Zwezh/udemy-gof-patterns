package by.zwezh.patterns.mediator;

public class Mediator {
    public Mediator() {
        Chat chat = new ChatMediator();
        Colegue alex = new ColegueImpl(chat, "Alex");
        Colegue vovan = new ColegueImpl(chat, "Vovan");
        Colegue serj = new ColegueImpl(chat, "Serj");
        Colegue mike = new ColegueImpl(chat, "Mike");
        chat.addColegue(alex);
        chat.addColegue(vovan);
        chat.addColegue(serj);
        chat.addColegue(mike);
        mike.sendMessage("From Mike");
        alex.sendMessage("From Alex");
        vovan.sendMessage("From Vovan");
    }
}
