package by.zwezh.patterns.state;

public abstract class AState {
    int count = 10;

    public abstract void insertQuarter(GumMachine gumMachine);
    public abstract void turnCrank(GumMachine gumMachine);
}
