package by.zwezh.patterns.state;

public class State {
    public State() {
        GumMachine gumMachine = new GumMachine();
        gumMachine.insertQuarter();
        gumMachine.insertQuarter();
        gumMachine.turnCrank();
    }
}
