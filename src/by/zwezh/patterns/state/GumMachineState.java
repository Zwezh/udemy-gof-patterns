package by.zwezh.patterns.state;

public enum GumMachineState {
    SOLD_OUT, NO_QUARTER, HAS_QUARTER, WINNER
}
