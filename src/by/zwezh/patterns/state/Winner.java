package by.zwezh.patterns.state;

public class Winner extends AState{
    @Override
    public void insertQuarter(GumMachine gumMachine) {
        System.out.println("You are winnner");
    }

    @Override
    public void turnCrank(GumMachine gumMachine) {
        System.out.println("Get gum");
        gumMachine.state = new NoQuarter();
    }
}
