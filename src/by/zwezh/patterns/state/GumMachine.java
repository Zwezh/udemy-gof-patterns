package by.zwezh.patterns.state;

public class GumMachine {
    AState state = new NoQuarter();

    public void insertQuarter() {
        state.insertQuarter(this);
    }

    public void turnCrank() {
        state.turnCrank(this);
    }
}
