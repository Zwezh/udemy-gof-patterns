package by.zwezh.patterns.state;

public class NoQuarter extends AState {
    @Override
    public void insertQuarter(GumMachine gumMachine) {
        gumMachine.state = new HasQuarter();
    }

    @Override
    public void turnCrank(GumMachine gumMachine) {
        System.out.println("On quarter");
    }
}
