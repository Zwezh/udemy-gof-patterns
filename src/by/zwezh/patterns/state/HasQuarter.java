package by.zwezh.patterns.state;

public class HasQuarter extends AState{
    @Override
    public void insertQuarter(GumMachine gumMachine) {
        System.out.println("You have set quarter already");
    }

    @Override
    public void turnCrank(GumMachine gumMachine) {
        if(count <= 0) {
            gumMachine.state = new SoldOut();
        } else {
            System.out.println("Celling...");
            count--;
            gumMachine.state = new NoQuarter();
        }
    }
}
