package by.zwezh.patterns.state;

public class SoldOut extends AState{
    @Override
    public void insertQuarter(GumMachine gumMachine) {
        System.out.println("No gums");
    }

    @Override
    public void turnCrank(GumMachine gumMachine) {
        System.out.println("No gums");
    }
}
