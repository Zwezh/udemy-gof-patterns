package by.zwezh.patterns.template;

public class Coffee extends Bevarage {

    @Override
    protected void addBevarage() {
        System.out.println("Added coffee");
    }

    @Override
    protected void addConditment() {
        System.out.println("Added milk");
    }

    @Override
    public void hook() {
        System.out.println("Added syrop");
    }
}
