package by.zwezh.patterns.template;

public abstract class Bevarage {
    private void boilWater() {
        System.out.println("Boiled water");
    }

    protected abstract void addBevarage();

    private void addSugar() {
        System.out.println("Added sugar");
    }

    protected abstract void addConditment();

    public void makeBevarage() {
        boilWater();
        addBevarage();
        addSugar();
        addConditment();
        hook();
    }

    public void hook() {

    }
}
