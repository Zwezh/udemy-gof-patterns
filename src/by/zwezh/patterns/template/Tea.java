package by.zwezh.patterns.template;

public class Tea extends Bevarage {
    private void boilWater() {
        System.out.println("Boiled water");
    }

    @Override
    protected void addBevarage() {
        System.out.println("Added tea");
    }

    @Override
    protected void addConditment() {
        System.out.println("Added lemon");
    }
}
