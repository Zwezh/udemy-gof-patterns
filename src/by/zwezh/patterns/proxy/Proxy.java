package by.zwezh.patterns.proxy;

import java.lang.reflect.InvocationHandler;

public class Proxy {
    public Proxy() {
//        Reader reader = new ProxyReader();
//        reader.read("Hello");
        InvocationHandler invocationHandler = new ReaderInvocationHandler();
        Object proxyInstance =  java.lang.reflect.Proxy.newProxyInstance(Proxy.class.getClassLoader(), new Class[]{Reader.class}, invocationHandler);
        ((Reader)proxyInstance).read("Hello");
    }
}
