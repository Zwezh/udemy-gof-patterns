package by.zwezh.patterns.proxy;

public interface Reader {
    String read(String str);
}
