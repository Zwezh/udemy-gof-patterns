package by.zwezh.patterns.prototype;

public class Person implements Cloneable{
    String name;
    int age;
    Address address;

    public Person(String name, int age, Address address) {
        this.name = name;
        this.age = age;
        this.address = address;

    }

    public Person(Person person) {
        this.name = person.name;
        this.age = person.age;
        this.address = new Address(person.address);

    }

    @Override
    public Person clone() throws CloneNotSupportedException {
        Person person = (Person)super.clone();
        person.address = person.address.clone();
        return person;
    }
}
