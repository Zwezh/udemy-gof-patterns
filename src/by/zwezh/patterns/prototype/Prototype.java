package by.zwezh.patterns.prototype;

public class Prototype {
    public Prototype() throws CloneNotSupportedException {
        Person person = new Person("Mike", 22, new Address("Str", 2));
        Person person1 = new Person(person);
        System.out.println(person != person1);
        System.out.println(person.address != person1.address);
        System.out.println(person.name == person1.name);
    }
}
