package by.zwezh.patterns.decorator;

public class PepperoniPizza implements Pizza {

    private Pizza pizza;

    public PepperoniPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String makePizza() {
        return pizza.makePizza() + "pepperoni ";
    }
}
