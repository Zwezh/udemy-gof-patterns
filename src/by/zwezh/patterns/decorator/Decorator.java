package by.zwezh.patterns.decorator;

public class Decorator {
    public Decorator() {
        Pizza pizza = new CheasePizza(new DoughPizza());
        Pizza pizza1 = new CheasePizza(new PepperoniPizza(new DoughPizza()));
        Pizza pizza2 = new CheasePizza(new PepperoniPizza(new MashroomPizza(new TomatoPizza(new DoughPizza()))));
        System.out.println(pizza.makePizza());
        System.out.println(pizza1.makePizza());
        System.out.println(pizza2.makePizza());
    }
}
