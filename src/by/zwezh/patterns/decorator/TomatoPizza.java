package by.zwezh.patterns.decorator;

public class TomatoPizza implements Pizza{
    private Pizza pizza;

    public TomatoPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String makePizza() {
        return pizza.makePizza() + "tomato ";
    }
}
