package by.zwezh.patterns.decorator;

public class MashroomPizza implements Pizza{
    Pizza pizza;

    public MashroomPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String makePizza() {
        return pizza.makePizza() + "mashrooms ";
    }
}
