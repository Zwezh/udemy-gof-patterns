package by.zwezh.patterns.decorator;

public interface Pizza {
    String makePizza();
}
