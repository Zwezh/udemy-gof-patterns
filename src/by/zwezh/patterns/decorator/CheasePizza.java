package by.zwezh.patterns.decorator;

public class CheasePizza implements Pizza {
    Pizza pizza;

    public CheasePizza(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String makePizza() {
        return pizza.makePizza() + "chease ";
    }
}
