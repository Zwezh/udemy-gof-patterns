package by.zwezh.patterns.iterator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Menu {
//    String[] items = {"one", "two", "three", "four", "five", "six"};
    List<String> items = new ArrayList<>();

    public Menu() {
        items.add("one");
        items.add("two");
        items.add("three");
        items.add("four");
        items.add("five");
        items.add("six");
    }

    Iter<String> getIterator() {
        return new Iter<String>() {
            int i;
            @Override
            public boolean hasNext() {
                return i < items.size();
            }

            @Override
            public String next() {
                return items.get(i++);
            }
        };
    }
}
