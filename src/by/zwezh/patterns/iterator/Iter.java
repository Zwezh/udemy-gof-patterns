package by.zwezh.patterns.iterator;

public interface Iter<T> {

    boolean hasNext();

    T next();
}
