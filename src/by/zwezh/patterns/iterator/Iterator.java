package by.zwezh.patterns.iterator;

public class Iterator {
    public Iterator() {
        Menu menu = new Menu();
        Iter<String> iterator = menu.getIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
