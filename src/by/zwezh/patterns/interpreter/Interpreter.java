package by.zwezh.patterns.interpreter;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Interpreter {
    public Interpreter() {
        String exp = "a + b";
        Expr parse = parse(exp);
        Map<String ,Integer> context = new HashMap<>();
        context.put("a", 10);
        context.put("b", 31);
        int result = parse.interpret(context);
        System.out.println(result);
    }

    private Expr parse(String exp) {
        ArrayDeque<Expr> stack = new ArrayDeque<Expr>();
        Arrays.stream(exp.split(" ")).filter(token -> Character.isLetter(token.charAt(0))).forEach(token -> {
            stack.push(parseToken(token, stack));
        });
        Arrays.stream(exp.split(" ")).filter(token -> !Character.isLetter(token.charAt(0))).forEach(token -> {
            stack.push(parseToken(token, stack));
        });
        return stack.pop();
    }

    private Expr parseToken(String token, ArrayDeque<Expr> stack) {
        Expr left, right;
        switch (token) {
            case "+":
                right = stack.pop();
                left = stack.pop();
                return Expr.plus(left, right);
            default:
                return Expr.variable(token);
        }
    }
}
