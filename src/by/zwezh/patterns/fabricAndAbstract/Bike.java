package by.zwezh.patterns.fabricAndAbstract;

public interface Bike {
    void drive();
}
