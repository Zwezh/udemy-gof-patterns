package by.zwezh.patterns.fabricAndAbstract;

public class BmwFactory extends CarFactory {
    @Override
    Car getCar() {
        return new Bmw();
    }
}
