package by.zwezh.patterns.fabricAndAbstract;

public class MercedesBike implements Bike {
    @Override
    public void drive() {
        System.out.println("Drive mercedes bike");
    }
}
