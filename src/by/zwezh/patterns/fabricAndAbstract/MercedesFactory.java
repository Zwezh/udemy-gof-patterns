package by.zwezh.patterns.fabricAndAbstract;

public class MercedesFactory extends CarFactory {
    @Override
    Car getCar() {
        return new Mercedes();
    }
}
