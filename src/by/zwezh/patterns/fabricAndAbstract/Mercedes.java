package by.zwezh.patterns.fabricAndAbstract;

public class Mercedes implements Car{
    @Override
    public void drive() {
        System.out.println("Drive mercedes");
    }
}
