package by.zwezh.patterns.fabricAndAbstract;

public class Bmw implements Car{
    @Override
    public void drive() {
        System.out.println("Drive Bmw");
    }
}
