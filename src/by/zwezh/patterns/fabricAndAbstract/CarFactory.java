package by.zwezh.patterns.fabricAndAbstract;

public abstract class CarFactory {
    public void createCar() {
        Car car = getCar();
        car.drive();
    }

    abstract Car getCar();
}
