package by.zwezh.patterns.fabricAndAbstract;

public interface AbstractFactory {
    Car getCar();
    Bike getBike();
}
