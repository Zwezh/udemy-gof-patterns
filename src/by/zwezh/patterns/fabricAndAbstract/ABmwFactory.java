package by.zwezh.patterns.fabricAndAbstract;

public class ABmwFactory implements AbstractFactory{
    @Override
    public Car getCar() {
        return new Bmw();
    }

    @Override
    public Bike getBike() {
        return new BmwBike();
    }
}
