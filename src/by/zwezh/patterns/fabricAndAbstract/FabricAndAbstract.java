package by.zwezh.patterns.fabricAndAbstract;

public class FabricAndAbstract {
    public FabricAndAbstract() {
        CarFactory carFactory = new MercedesFactory();
        carFactory.createCar();
        AbstractFactory abstractFactory = new AMercedesFactory();
        Car mercedes = abstractFactory.getCar();
        Bike mBike = abstractFactory.getBike();
        mercedes.drive();
        mBike.drive();
        AbstractFactory abstractFactory1 = new ABmwFactory();
        abstractFactory1.getBike().drive();
        abstractFactory1.getCar().drive();
    }
}
