package by.zwezh.patterns.fabricAndAbstract;

public interface Car {
    void drive();
}
