package by.zwezh.patterns.fabricAndAbstract;

public class BmwBike implements Bike {
    @Override
    public void drive() {
        System.out.println("Drive Bmw bike");
    }
}