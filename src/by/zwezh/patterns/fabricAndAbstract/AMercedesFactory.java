package by.zwezh.patterns.fabricAndAbstract;

public class AMercedesFactory implements AbstractFactory{
    @Override
    public Car getCar() {
        return new Mercedes();
    }

    @Override
    public Bike getBike() {
        return new MercedesBike();
    }
}
