package by.zwezh.patterns.composit;

public class Composit {
    public Composit() {
        Folder users = new Folder("users");
        Folder etc = new Folder("etc");
        Folder home = new Folder("home");
        Folder root = new Folder("root");
        root.addFolder(users, etc, home);

        Folder mike = new Folder("Mike");
        Folder kent = new Folder("Kent");
        Folder max = new Folder("Max");
        users.addFolder(max, mike, kent);

        Folder one = new Folder("One");
        Folder two = new Folder("Two");
        Folder three = new Folder("Three");
        Folder four = new Folder("Four");
        mike.addFolder(one, two, three, four);
        root.printFolders();
    }
}
