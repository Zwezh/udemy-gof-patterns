package by.zwezh.patterns.composit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Folder {
    private String name;
    private List<Folder> list;

    public Folder(String name) {
        this.name = name;
        list = new ArrayList<>();
    }

    public void addFolder(Folder folder) {
        list.add(folder);
    }

    public void addFolder(Folder... folders) {
        list.addAll(Arrays.asList(folders));
    }

    public void printFolders() {
        for (Folder folder: list) {
            System.out.println(folder.name);
            folder.printFolders();
        }
    }
}
