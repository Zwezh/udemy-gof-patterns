package by.zwezh.patterns.adapter;

public class Adapter {
    public Adapter() {
        AmericanSocket americanSocket = new SimpleAmericanSocket();
        Radio radio = new Radio();
        EuroSocket euroSocket = new SocketAdapter(americanSocket);
        radio.listenToMusic(euroSocket);
    }
}
