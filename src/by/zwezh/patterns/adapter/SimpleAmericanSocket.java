package by.zwezh.patterns.adapter;

public class SimpleAmericanSocket implements AmericanSocket{
    @Override
    public void getPower() {
        System.out.println("Get 110 volts");
    }
}