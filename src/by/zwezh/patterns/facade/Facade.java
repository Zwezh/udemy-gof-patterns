package by.zwezh.patterns.facade;

public class Facade {
    public Facade() {
        LivingRoom livingRoom = new LivingRoom();
        livingRoom.pressButton("4", "20");
        BedRoom bedRoom = new BedRoom();
        bedRoom.pressButton("4", "20");
    }
}
