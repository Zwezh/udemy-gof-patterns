package by.zwezh.patterns.facade;

public class AirConditioning {
    void setTemperature(String temperature) {
        System.out.println("Set temperature " + temperature);
    }
}
