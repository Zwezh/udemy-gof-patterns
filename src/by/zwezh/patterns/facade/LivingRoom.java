package by.zwezh.patterns.facade;

public class LivingRoom {

    private RoomFacade roomFacade = new RoomFacade();

    public void pressButton(String channel, String temperature) {
        roomFacade.pressButton(channel, temperature);
    }
}
