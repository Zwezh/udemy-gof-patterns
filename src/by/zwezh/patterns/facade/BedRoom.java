package by.zwezh.patterns.facade;

public class BedRoom {

    private RoomFacade roomFacade = new RoomFacade();

    public void pressButton(String channel, String temperature) {
        roomFacade.pressButton(channel, temperature);
    }
}
