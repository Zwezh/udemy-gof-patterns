package by.zwezh.patterns.facade;

public class Light {
    void turnLight() {
        System.out.println("Run light");
    }
}
