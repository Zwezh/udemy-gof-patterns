package by.zwezh.patterns.facade;

public class Tv {
    void playChannel(String channel) {
        System.out.println("Play channel " + channel);
    }
}
