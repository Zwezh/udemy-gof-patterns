package by.zwezh.patterns.flyWeight;

public class StudentUniversityInfo {
    String facilty;
    String universityCity;
    Hostel hostelAddress;

    public StudentUniversityInfo(String facilty, String universityCity, Hostel hostelAddress) {
        this.facilty = facilty;
        this.universityCity = universityCity;
        this.hostelAddress = hostelAddress;
    }
}
