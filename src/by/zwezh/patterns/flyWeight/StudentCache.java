package by.zwezh.patterns.flyWeight;

import java.util.WeakHashMap;

public class StudentCache {
    private static final WeakHashMap<String, StudentUniversityInfo> studentUniversityInfos = new WeakHashMap<>();

    public StudentUniversityInfo getStudentUniversityInfo(String name) {
        StudentUniversityInfo studentUniversityInfo = studentUniversityInfos.get(name);
        if (studentUniversityInfo == null) {
            studentUniversityInfo = createStudentInfo(name);
            studentUniversityInfos.put(name, studentUniversityInfo);
        }
        return studentUniversityInfo;
    }

    private StudentUniversityInfo createStudentInfo(String faculty) {
        switch (faculty) {
            case "informatic":
                return new StudentUniversityInfo(faculty, "New York", new Hostel());
            case "mathematic":
                return new StudentUniversityInfo(faculty, "Boston", new Hostel());
            case "management":
                return new StudentUniversityInfo(faculty, "Los Angeles", new Hostel());
            case "engineering":
                return new StudentUniversityInfo(faculty, "California", new Hostel());
            default:
                throw new IllegalArgumentException("no faculty");
        }
    }
}
