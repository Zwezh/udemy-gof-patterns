package by.zwezh.patterns.flyWeight;

public class FlyWeight {
    public FlyWeight() {
PersonCache cache = new PersonCache();
        Person mike = cache.getPerson("Mike");
        cache.getPerson("Nick");
        Person mike2 = cache.getPerson("Mike");
        System.out.println(mike == mike2);
        StudentCache studentCache = new StudentCache();
        StudentUniversityInfo management = studentCache.getStudentUniversityInfo("management");
        StudentUniversityInfo informatic = studentCache.getStudentUniversityInfo("informatic");
        StudentUniversityInfo management1 = studentCache.getStudentUniversityInfo("management");
        StudentUniversityInfo informatic1 = studentCache.getStudentUniversityInfo("informatic");
//        StudentUniversityInfo informatic3 = studentCache.getStudentUniversityInfo("informatic3");
        System.out.println(management == management1);
        System.out.println(management == informatic);
        System.out.println(informatic == informatic);
        System.out.println(informatic == informatic1);
    }
}
