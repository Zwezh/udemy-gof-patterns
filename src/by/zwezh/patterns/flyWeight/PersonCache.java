package by.zwezh.patterns.flyWeight;

import java.util.WeakHashMap;

public class PersonCache {
    private final WeakHashMap<String, Person> persons;

    public PersonCache() {
        persons = new WeakHashMap<String, Person>();
    }

    public Person getPerson(String name) {
        Person person = persons.get(name);
        if (person == null) {
            person = new Person(name);
            persons.put(name, person);
        }
        return person;
    }
}
